# FastAPI의 미들웨어: 기본 구현부터 경로 기반 전략까지 (Part 1)

## 미들웨어는 어떤 역할을 하는가?

- **들어오는 요청**: 요청이 들어오면 미들웨어는 실제 API 서비스 메서드에 도달하기 전에 쿠키, 헤더, 쿼리 매개변수 등과 같은 요청의 일부를 검사하고 수정할 수도 있다. 예를 들어 요청의 유효성을 검사하거나, 사용자를 인증하거나, 요청 세부 정보를 기록할 수 있다.
- **내보내는 응답**: API 서비스 메서드가 요청을 처리하고 응답을 생성한 후 미들웨어는 응답이 클라이언트로 돌아가기 전에 응답을 검사하고 수정할 기회를 갖는다. 여기에는 응답 헤더 변경, 콘텐츠 변환 등이 포함될 수 있다.

두 미들웨어가 있는 예제를 만들어 보겠다.

1. `log_request_details`: 이 미들웨어는 들어오는 각 요청의 세부 정보를 파일에 기록한다.
1. `api_key_authentication`: 이 미들웨어는 요청 헤더에 유효한 API 키가 있는지 확인한다.

이러한 미들웨어들은 정의하는 순서가 중요하다. 가장 먼저 정의된 미들웨어는 들어오는 요청을 가장 먼저 수신하고 나가는 응답을 수정하는 마지막 미들웨어이다.

코드는 다음과 같다.

```python
from fastapi import FastAPI, Request, HTTPException
from datetime import datetime

app = FastAPI()

API_KEY = "my_secret_api_key"

# First Middleware: Logging request details
@app.middleware("http")
async def log_request_details(request: Request, call_next):
    method_name = request.method
    path = request.url.path
    with open("request_log.txt", mode="a") as reqfile:
        content = f"Method: {method_name}, Path: {path}, Received at: {datetime.now()}\n"
        reqfile.write(content)
    
    response = await call_next(request)
    return response

# Second Middleware: API key authentication
@app.middleware("http")
async def api_key_authentication(request: Request, call_next):
    api_key = request.headers.get("X-Api-Key")
    if api_key != API_KEY:
        raise HTTPException(status_code=401, detail="Unauthorized: Invalid API Key")
    
    response = await call_next(request)
    return response

@app.get("/")
async def read_root():
    return {"message": "Hello, World!"}

@app.get("/another")
async def read_another():
    return {"message": "This is another endpoint"}
```

FastAPI 어플리케이션에 등록된 미들웨어는 대상 엔드포인트에 관계없이 들어오는 모든 HTTP 요청에 대하여 호출된다. 이는 미들웨어의 핵심 기능 중 하나로, 모든 엔드포인트에서 요청을 전처리하고 응답을 후처리하기 위한 공통 로직을 실행할 수 있다.

1. `/` 또는 `/another` 엔드포인트에 요청이 이루어지면 `log_request_details` 미들웨어가 먼저 요청 세부 정보를 캡처하고 기록한다.
1. 다음 `api_key_authentication` 미들웨어가 요청 헤더에 유효한 API 키가 있는지 확인한다.
1. API 키가 유효하면 요청을 타겟 엔드포인트(`read_root` 또는 `read_another`)로 라우팅한다.
1. 다음 엔드포인트의 응답은 클라이언트로 다시 전송되기 전에 `api_key_authentication`과 `log_request_details` 미들웨어(순서대로)를 거치게 된다.

이 순서는 FastAPI 어플리케이션의 모든 엔드포인트에 대한 모든 요청에 대해 동일하게 아루진다.

`call_next`라는 이름은 어려운 요구 사항이 아니라 규칙이다. 기술적으로는 원하는 이름을 지정할 수 있다. 하지만 미들웨어 함수의 두 번째 매개변수는 `Request` 객체를 받아 `Response`를 반환하는 콜러블(callable)이어야 한다. 이 콜러블은 일반적으로 FastAPI 자체에서 제공하므로 구현에 대해 걱정할 필요가 없다. `call_next`를 이름으로 사용하면 이 함수가 요청을 파이프라인의 다음 단계로 전달할 책임이 있음을 명확히 알 수 있다.

FastAPI 어플리케이션에 요청이 들어오면 엔드포인트(API 서비스 메서드)로 바로 이동하지 않는다. 먼저 등록된 모든 미들웨어를 거친다. 각 미들웨어는 요청을 수정하거나, 일부 작업을 수행하거나, 심지어 요청을 완전히 거부할 수도 있다.

"요청 전달"이라는 용어는 현재 미들웨어에서 스택의 다음 미들웨어로 요청 객체를 전달하는 것을 의미한다. 현재 미들웨어가 스택의 마지막 미들웨어인 경우 요청은 비즈니스 로직이 있는 실제 엔드포인트(예: 사용자가 정의한 GET, POST 메서드)로 전달된다.

다음은 이를 설명하기 위해 단순화시킨 시퀀스이다.

1. 요청이 도착한다.
1. 미들웨어 A가 요청을 처리한다. `call_next`를 호출하여 요청을 다음 단계로 전달한다.
1. 미들웨어 B가 요청을 처리한다. `call_next`를 호출하여 요청을 다음 단계로 전달한다.
1. 요청이 엔드포인트(API 메서드)에 도달하면 응답이 생성된다.
1. 응답은 미들웨어 B를 통해 반환되며, 미들웨어 B는 응답을 수정할 수 있다.
1. 응답은 미들웨어 A를 통해 반환되며, 미들웨어 A도 응답을 수정할 수 있다.
1. 응답이 클라이언트로 다시 반환된다.

따라서 미들웨어 내에서 `call_next(request)`를 호출하면 "이 요청을 끝냈으니 다음 미들웨어나 엔드포인트로 전달하세요."라고 말하는 것이다. 그런 다음 엔드포인트가 요청을 처리하고 응답을 생성하면 제어권이 미들웨어로 돌아와 클라이언트로 다시 전송되기 전에 응답을 수정할 수 있다.

FastAPI의 `@app.middleware("http")` 데코레이터를 사용하지 않고도 유사한 기능을 구현할 수 있지만, 각 엔드포인트 함수 내에서 요청과 응답 흐름을 수동으로 처리해야 한다. 이렇게 하면 코드를 관리하고 유지 관리하기가 더 어려워질 수 있다.

## 수동 요청과 응답 처리
예를 들어 모든 API 엔드포인트에 적용하려는 다음 두 기능이 있다고 가정해 보자.

- 요청 세부 정보 로깅.
- 유효한 API 키 확인.

이러한 기능을 일반 Python 함수로 정의한 다음 모든 FastAPI 엔드포인트 내에서 호출할 수 있다.

```python
from fastapi import FastAPI, Request, HTTPException

app = FastAPI()
API_KEY = "my_secret_api_key"
def log_request_details(request: Request):
    # Your logging logic here
    pass
def check_api_key(request: Request):
    api_key = request.headers.get("X-Api-Key")
    if api_key != API_KEY:
        raise HTTPException(status_code=401, detail="Unauthorized: Invalid API Key")

@app.get("/")
async def read_root(request: Request):
    log_request_details(request)
    check_api_key(request)
    return {"message": "Hello, World!"}

@app.get("/another-endpoint")
async def another_endpoint(request: Request):
    log_request_details(request)
    check_api_key(request)
    return {"message": "This is another endpoint"}
```

## 단점

1. **반복**: 모든 엔드포인트에서 `log_request_details()`과 `check_api_key()`를 호출하는 것을 기억해야 한다. 이 때문에 잊거나 실수하기 쉽다.
1. **중앙 집중식 로직의 부족**: 로깅 또는 API 키 확인의 작동 방식을 변경하려면 이 로직을 구현한 모든 엔드포인트를 업데이트해야 한다.
1. **유지보수성**: API가 성장함에 따라 코드베이스를 관리하고 유지 관리하기가 점점 더 어려워질 수 있다.

미들웨어와 유사한 동작을 모든 엔드포인트에 전역적으로 적용하지 않고 특정 엔드포인트에만 적용하려는 시나리오가 있을 수 있다. 이를 흔히 "경로 기반 미들웨어(route-based middleware)"라고 한다.

## 경로 기반 미들웨어 사용 사례

1. **권한 부여(Authorization)**: 공용 엔드포인트와 비공개 엔드포인트가 있을 수 있으며, 비공개 엔드포인트에 대해서만 권한 부여 검사를 적용하고 싶을 수 있다.
1. **속도 제한(Rate Limiting)**: 특정 리소스 집약적인 엔드포인트에만 속도 제한을 적용하고 싶을 수 있다.
1. **로깅**: 디버깅 또는 모니터링 목적으로 특정 엔드포인트에 대한 자세한 로깅을 원할 수 있다.

## 경로 기반 미들웨어를 위한 접근 방식
FastAPI는 경로 기반 미들웨어를 기본적으로 지원하지 않지만 몇 가지 다른 방법으로 이 기능을 구현할 수 있다.

**FastAPI 종속성**: 미들웨어와 유사한 작업을 수행하는 종속성 함수를 정의하여 필요한 경로에만 포함시킬 수 있다.

```python
from fastapi import FastAPI, Request, Depends, HTTPException

app = FastAPI()

async def verify_api_key(request: Request):
    api_key = request.headers.get("X-Api-Key")
    if api_key != "my_secret_api_key":
        raise HTTPException(status_code=401, detail="Invalid API Key")

@app.get("/", dependencies=[Depends(verify_api_key)])
async def read_root():
    return {"message": "Hello, World!"}
```

**데코레이터 함수**: 경로 함수를 감싸는 Python 데코레이터를 정의할 수도 있다. 하지만 이 방법은 FastAPI 종속성을 사용하는 것만큼 깔끔하거나 관용적이지 않을 수 있다.

```python
from functools import wraps

def verify_api_key_decorator(route_function):
    @wraps(route_function)
    async def wrapper(*args, **kwargs):
        request = kwargs.get("request")
        api_key = request.headers.get("X-Api-Key")
        if api_key != "my_secret_api_key":
            return {"error": "Invalid API Key"}
        return await route_function(*args, **kwargs)
    return wrapper

@app.get("/secure")
@verify_api_key_decorator
async def secure_endpoint(request: Request):
    return {"message": "Secure data"}
```

이러한 접근 방식 중 하나를 사용하면 특정 경로에만 미들웨어 로직을 적용하여 요청과 응답 처리를 보다 세밀하게 제어할 수 있다.
