# FastAPI의 미들웨어 

FastAPI의 미들웨어 개념은 REST API를 통해 이동하는 HTTP 요청과 응답을 필터링하고 처리하는 데 사용된다. 미들웨어는 기본적으로 클라이언트(요청을 하는 쪽)와 API 서비스(요청을 처리하는 쪽) 사이에 있는 계층이다.

- [기본 구현부터 경로 기반 전략까지 (Part 1)](./middleware-in-fastapi-part1.md)<sup>[1](#footnote_1)</sup>

<a name="footnote_1">1</a>: 이 페이지는 [Middleware in FastAPI: From Basic Implementation to Route-Based Strategies (Part 1)](https://medium.com/@saverio3107/mastering-middleware-in-fastapi-from-basic-implementation-to-route-based-strategies-d62eff6b5463)를 편역한 것이다.
